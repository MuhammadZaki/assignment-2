﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingGameTest

{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void SetUp()
        {
            game = new Game();
        }

        public void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }
        [Test]
        public void RollGutterGame()
        {
            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void RollSpareFirstFrame()
        {
            game.Roll(9); game.Roll(1);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));
        }

        [Test]
        public void RollStrikeFirstFrame()
        {
            game.Roll(10);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(30));
        }

        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }

        [Test]
        public void RollSpareEveryFrame()
        {
            RollMany(21, 5);
            Assert.That(game.Score(), Is.EqualTo(150));
        }

        [Test]
        public void RollTwos()
        {
            RollMany(20, 2);
            Assert.That(game.Score(), Is.EqualTo(40));
        }

        [Test]
        public void RollThrees()
        {
            RollMany(20, 3);
            Assert.That(game.Score(), Is.EqualTo(60));
        }
    }
}
