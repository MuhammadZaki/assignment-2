﻿using System;

namespace BowlingGame
{
    public class Game
    {
        int[] pinsFall = new int[21];
        int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll(int pins)
        {
            pinsFall[rollCounter] = pins;
            rollCounter++;
        }

        public int Score()
        {
            int i = 0;
            int score = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                if(pinsFall[i] == 10)
                {
                    score += 10 + pinsFall[i + 1] + pinsFall[i + 2];
                    i += 1;
                }
                else if(pinsFall[i] + pinsFall[i + 1] == 10)
                {
                    score += 10 + pinsFall[i + 2];
                    i += 2;
                }
                else
                {
                    score += pinsFall[i] + pinsFall[i + 1];
                    i += 2;
                }
            }

            return score;
        }

    }
}
